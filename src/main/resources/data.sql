DROP TABLE IF EXISTS tableau;

CREATE TABLE tableau (
  id INT PRIMARY KEY,
  valeur INT NOT NULL
);

INSERT INTO tableau (id, valeur) VALUES
  (1, 1),
  (5, 2),
  (7, 3),
  (9, 4),
  (13, 5),
  (15, 6),
  (19, 7),
  (21, 8),
  (23, 9);
