package com.example.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tableau")
public class Tableau {

	@Id
	private Integer id;

	private Integer valeur;

	public Tableau (Integer id, Integer valeur) {
		this.id = id;
		this.valeur = valeur;
	}

	public Tableau() { }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getValeur() {
		return valeur;
	}

	public void setValeur(Integer valeur) {
		this.valeur = valeur;
	}

	@Override
	public String toString() {
		return "Tableau{" +
				"id=" + id +
				", valeur=" + valeur +
				'}';
	}
}
