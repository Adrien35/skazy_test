package com.example.controller;

import com.example.jpa.TableauRepository;
import com.example.jpa.TableauService;
import com.example.model.Tableau;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class SkazyController {

    @Autowired
    TableauService tableauService;

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.GET, value = "/getResults", produces = "application/json")
    public List<Tableau> getResults() {
        //je retourne seulement une des 128 possibilités
        return calcul().get((int)(1 + (Math.random() * (127 - 1))));
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.GET, value = "/getValues", produces = "application/json")
    private List<Tableau> getAllValue() {
        return tableauService.getAllValue();
    }


    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.PUT, value = "/modifyValue/{id}", produces = "application/json")
    private void modifyValue(@RequestBody Tableau tableau) {
        tableauService.modifyValue(tableau);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.POST, value = "/modifyAllValue", produces = "application/json")
    private void modifyAllValue(@RequestBody List<Tableau> values) {
        tableauService.modifyAllValue(values);
    }


    List<List<Tableau>> calcul() {

        List<List<Tableau>> listTab = new ArrayList<>();

            for (int i1 = 1; i1 <= 9; i1++) {

                for (int i5 = 1; i5 <= 9; i5++) {
                    if (i5 != i1) {

                        for (int i7 = 1; i7 <= 9; i7++) {
                            if (i7 != i1 && i7 != i5) {

                                for (int i9 = 1; i9 <= 9; i9++) {
                                    if (i9 != i1 && i9 != i5 && i9 != i7) {

                                        for (int i13 = 1; i13 <= 9; i13++) {
                                            if (i13 != i1 && i13 != i5 && i13 != i7 && i13 != i9) {

                                                for (int i15 = 1; i15 <= 9; i15++) {
                                                    if (i15 != i1 && i15 != i5 && i15 != i7 && i15 != i9 && i15 != i13) {

                                                        for (int i19 = 1; i19 <= 9; i19++) {
                                                            if (i19 != i1 && i19 != i5 && i19 != i7 && i19 != i9 && i19 != i13 && i19 != i15) {

                                                                for (int i21 = 1; i21 <= 9; i21++) {
                                                                    if (i21 != i1 && i21 != i5 && i21 != i7 && i21 != i9 && i21 != i13 && i21 != i15 && i21 != i19) {

                                                                        for (int i23 = 1; i23 <= 9; i23++) {
                                                                            if (i23 != i1 && i23 != i5 && i23 != i7 && i23 != i9 && i23 != i13 && i23 != i15 && i23 != i19 && i23 != i21) {

                                                                                if ((i1 + ((13.0 * i5) / (double)i7) + i9 + (12.0 * i13) - i15 - 11.0 + ((i19 * i21) / (double)i23) - 10.0) == 66.0) {
                                                                                    List<Tableau> values = new ArrayList<>();
                                                                                    Tableau t0 = new Tableau(0, i1);
                                                                                    Tableau t1 = new Tableau(1, i5);
                                                                                    Tableau t2 = new Tableau(2, i7);
                                                                                    Tableau t3 = new Tableau(3, i9);
                                                                                    Tableau t4 = new Tableau(4, i13);
                                                                                    Tableau t5 = new Tableau(5, i15);
                                                                                    Tableau t6 = new Tableau(6, i19);
                                                                                    Tableau t7 = new Tableau(7, i21);
                                                                                    Tableau t8 = new Tableau(8, i23);
                                                                                    values.add(t0);
                                                                                    values.add(t1);
                                                                                    values.add(t2);
                                                                                    values.add(t3);
                                                                                    values.add(t4);
                                                                                    values.add(t5);
                                                                                    values.add(t6);
                                                                                    values.add(t7);
                                                                                    values.add(t8);
                                                                                    listTab.add(values);
                                                                                    break;
                                                                                }

                                                                            }
                                                                        }

                                                                    }
                                                                }

                                                            }
                                                        }

                                                    }
                                                }

                                            }
                                        }

                                    }
                                }

                            }
                        }

                    }
                }

            }


//            for(int i=0; i<listTab.size(); i++) {
//                System.out.println(listTab.get(i));
//                System.out.println(i);
//            }
//        128 possibilités

        return listTab;
    }



}
