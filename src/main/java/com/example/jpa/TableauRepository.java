package com.example.jpa;

import com.example.model.Tableau;
import org.springframework.data.repository.CrudRepository;

public interface TableauRepository extends CrudRepository<Tableau, Integer> {

}
