package com.example.jpa;

import com.example.model.Tableau;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TableauService {

    @Autowired
    TableauRepository tableauRepository;

    public List<Tableau> getAllValue() {
        List<Tableau> values = new ArrayList<>();
        tableauRepository.findAll().forEach(value -> values.add(value));
        return values;
    }

    public void modifyValue(Tableau tableau) {
        tableauRepository.save(tableau);
    }

    public void modifyAllValue(List<Tableau> values) {
        for (Tableau tableau: values) {
            tableauRepository.save(tableau);
        }
    }
}
