import { BrowserModule } from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import { LayoutModule } from '@angular/cdk/layout';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";
import { CarouselModule } from 'ngx-bootstrap/carousel';

//module Material et Bootstrap
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MaterialModule} from "./material.module";
import {ReactiveFormsModule} from '@angular/forms';

import {AccueilComponent} from './public/accueil/accueil.component';
import { UiModule } from './ui/ui.module';

import {MatDialogModule} from "@angular/material/dialog";

import {UtilsService} from "./utilsServices";
import {TableauService} from "./public/accueil/tableau.service";




const appRoutes: Routes = [
    { path: '', component: AccueilComponent },
    // otherwise redirect to home
    // { path: '**', redirectTo: '' }

];

@NgModule({
    declarations: [
        AppComponent,
        AccueilComponent

    ],
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        LayoutModule,
        NgbModule,
        MaterialModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes),
        FormsModule,
        UiModule,
        MatDialogModule,
        CarouselModule.forRoot()
    ],
    providers: [
        UtilsService,
        TableauService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
