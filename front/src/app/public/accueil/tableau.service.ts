import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Router} from "@angular/router";
import 'rxjs/add/operator/map'

@Injectable()
export class TableauService {
    constructor(private http: HttpClient, private router: Router) {   }
    url = 'http://localhost:8080';

    getResults() {
        return this.http.get(`${this.url}/getResults`);
    }


    getValues() {
        return this.http.get(`${this.url}/getValues`);
    }

    modifyValue(id, line) {
        return this.http.put(`${this.url}//modifyValue/`+id, line);
    }


    modifyAllValue() {
        return this.http.post(`${this.url}//modifyAllValue`,
            [
                {"id":1, "valeur": 1},
                {"id":5, "valeur": 1},
                {"id":7, "valeur": 1},
                {"id":9, "valeur": 1},
                {"id":13, "valeur": 1},
                {"id":15, "valeur": 1},
                {"id":19, "valeur": 1},
                {"id":21, "valeur": 1},
                {"id":23, "valeur": 1},
                ]);
    }


}
