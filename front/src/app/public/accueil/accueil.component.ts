import {Component, OnInit} from '@angular/core';
import {UtilsService} from "../../utilsServices";
import { Observable } from 'rxjs/Observable';
import {Router} from "@angular/router";
import {TableauService} from "./tableau.service";
import {Tableau} from "./tableau.interface";

@Component({
    selector: 'app-accueil',
    templateUrl: './accueil.component.html',
    styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {

    tabs: Tableau[];
    timeLeft: number = 0;
    interval;

    constructor(private utilsService: UtilsService, private tableauService: TableauService, private router: Router) {

    }

    ngOnInit() {
        this.getValues();
    }

    getValues() {
        this.tableauService.getValues().subscribe((data: Tableau[]) => {
            this.tabs = data;
        });
    }

    modifyValue(id, line) {
        this.tableauService.modifyValue(id, line).subscribe();
    }

    modifyAllValue() {
        this.tableauService.modifyAllValue().subscribe();
        window.location.reload();
    }

    doCalcul() {
        this.interval = setInterval(() => {
            this.timeLeft++;
        },1000);

        this.tableauService.getResults().subscribe((data: Tableau[]) => {
            this.tabs = data;
            clearInterval(this.interval);
        });
    }

    calculate(id0, id1, id2, id3, id4, id5, id6, id7, id8) {
        return Number(Number(id0.value) + (13 * Number(id1.value) / Number(id2.value)) + Number(id3.value) + (12*Number(id4.value)) - Number(id5.value) - 11 + (Number(id6.value) * Number(id7.value) / Number(id8.value)) - 10)
    }

}
