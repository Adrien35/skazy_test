#Access BDD H2
http://localhost:8080/h2-console
- url=jdbc:h2:mem:mytestdb
- driverClassName=org.h2.Driver
- username=sa
- password=


#Pour lancer l'application
- Run le fichier : src/main/java/com/example/DemoApplication.java
- Se rendre sur : localhost:8080


#Database 
/src/main/resources/data.sql

#Lancer la partie Angular sur le Tomcat
- ng build
- Copier le contenu du dist dans src/ressources/public
- Suivre les étapes pour lancer l'application
